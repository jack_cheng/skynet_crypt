# skynet_crypt

对接skynet登录服务器所需的加密插件的代码.使用“迪菲赫尔曼” 秘钥协商算法，保证服务器客户端之前不传输加密私钥，保证了通信的安全性



# 协商秘钥secret



```mermaid

sequenceDiagram
    participant client
    participant server
    server->>client: HMAC算法加密原始数据
    client->>+server: 发送A
    Note right of server: 服务端生成B
    server->>-client:  发送B
    Note over server,client: 根据计算指数生成秘钥
    client->>server: 使用HMAC算法，以DH算法为私钥，加密原始数据
    Note right of server: 验证秘钥对不对
    client->>+server: 发送加密过的token
    Note right of server: 验证用户是否合法
    server->>-client: 返回验证结果，如果成功返回200和直播间服务器的IP，端口和subid
    
    
    

```



## 使用



### android

​	不需要有额外的配置，只要引入插件就OK



### IOS

​	

​	需要将skynet_crypt/ios/Classes目录下的skynet_crypt.c文件导入到xcode当中，这样IOS编译的时候才能找到C扩展代码的函数
