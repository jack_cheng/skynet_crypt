import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:skynet_crypt/skynet_crypt.dart';

void main() {
  const MethodChannel channel = MethodChannel('skynet_crypt');

  TestWidgetsFlutterBinding.ensureInitialized();

  setUp(() {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      return '42';
    });
  });

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });

  test('getPlatformVersion', () async {
    expect(await SkynetCrypt.platformVersion, '42');
  });
}
